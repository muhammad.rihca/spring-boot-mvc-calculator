package com.InternJavan.CalculatorMVC.Service;

import java.text.DecimalFormat;

public class CalculatorService {

    public static String calculator(int valueA, int valueB, String type) {
        try {
            String result = "test cals";
            System.out.println(valueA + " a " + valueB + " b service" + " type" + type);
            double calcResult = 0;
            if (type.equalsIgnoreCase("+")) {
                calcResult = valueA + valueB;
            } else if (type.equalsIgnoreCase("-")) {
                calcResult = valueA - valueB;
            } else if (type.equalsIgnoreCase("X")) {
                calcResult = valueA * valueB;
            } else if (type.equalsIgnoreCase("/")) {
                if (valueB == 0) {
                    return "tidak bisa dilakukan";
                }
                calcResult = valueA / valueB;
            } else {
                result = "tidak bisa dilakukan";
            }

            DecimalFormat df = new DecimalFormat("#.##");
            return result = df.format(calcResult);
        } catch (Exception e) {
            return "There is something wrong!";
        }
    }
}
