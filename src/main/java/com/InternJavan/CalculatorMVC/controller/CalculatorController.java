package com.InternJavan.CalculatorMVC.controller;

import com.InternJavan.CalculatorMVC.Service.CalculatorService;
import com.InternJavan.CalculatorMVC.dto.NumberInfoDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class CalculatorController {

    private CalculatorService calculatorService;

    @RequestMapping("")
    public String result(NumberInfoDTO numberInfoDTO, Model model){
        String result = "";

        int valueA = numberInfoDTO.getNumber1();
        int valueB = numberInfoDTO.getNumber2();
        String type = numberInfoDTO.getType();

        if (type != null) {
            result = valueA + " " + type + " " + valueB + " = ";
            result += calculatorService.calculator(valueA, valueB, type);
            model.addAttribute("result", result);
        }
        return "index";
    }
}
